const router = require('express').Router();
let User = require('../models/user.model'); // this is moongo model we created

// if the URL is /users/ and it is a get request
router.route('/').get((req, res) => {
  User.find() //find() is a moongoes method that returns a promise
    .then(users => res.json(users))
    .catch(err => res.status(400).json('Error: ' + err));
});

// if the URL is /users/add and it is a post request
router.route('/add').post((req, res) => {
  const username = req.body.username; // sopys the post req into const called username

  const newUser = new User({username}); // creates a newUser using above created const

  newUser.save() //save() is a moongoes method
    .then(() => res.json('User added!'))
    .catch(err => res.status(400).json('Error: ' + err));
});

module.exports = router;

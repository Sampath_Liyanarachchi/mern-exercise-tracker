const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');

// this line so we can have our environment variables in .env file
require('dotenv').config();

//this is how we start our server
const app = express();
const port = process.env.PORT || 5000;

// this is our middleware
app.use(cors()); // this allows us to pass jason
app.use(express.json());

const uri = process.env.ATLAS_URI; // this is the URI we got from mongoDB Atlas
mongoose.connect(uri, { useNewUrlParser: true, useCreateIndex: true }
);
const connection = mongoose.connection;
connection.once('open', () => {
  console.log("MongoDB database connection established successfully");
})

const exercisesRouter = require('./routes/exercises');
const usersRouter = require('./routes/users');

// to include the routes in the route folder
app.use('/exercises', exercisesRouter);
app.use('/users', usersRouter);

app.listen(port, () => {
    console.log(`Server is running on port: ${port}`);
});

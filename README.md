Code for MERN tutorial.

Video Tutorial: https://www.youtube.com/watch?v=7CqJlxBYj-M

Article Tutorial: https://medium.com/@beaucarnes/learn-the-mern-stack-by-building-an-exercise-tracker-mern-tutorial-59c13c1237a1

===========================================
step 01: clone the repo
step 02: go inside the backend folder
step 03: npm i
step 04: nodemon server ( if nodemon is unaviliable -->   npm install -g nodemon  )
step 05: go back to root folder
step 06: npm i
step 07: npm start 
===========================================

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).
